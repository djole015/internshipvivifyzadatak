package restoran;

import java.util.ArrayList;
import java.util.List;

import restoran.jelo.Obrok;
import restoran.napitak.Pice;

public class Porudzbina {

	private Sto sto;

	private List<Obrok> obroci;

	private List<Pice> pica;

	private boolean isPaid;

	public Porudzbina(Sto sto) {
		super();
		this.sto = sto;
	}

	public Sto getSto() {
		return sto;
	}

	public void setSto(Sto sto) {
		this.sto = sto;
	}

	public List<Obrok> getObroci() {
		return obroci;
	}

	public void setObroci(List<Obrok> obroci) {
		this.obroci = obroci;
	}

	public List<Pice> getPica() {
		return pica;
	}

	public void setPica(List<Pice> pica) {
		this.pica = pica;
	}

	public boolean isPaid() {
		return isPaid;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

}
