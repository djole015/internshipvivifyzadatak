package restoran.jelo;

public class ObrokFactory {
	
	public static Obrok createObrok(ObrokType obrokType, String naziv) {
		return obrokType.getObrok(naziv);
	}
}
