package restoran.jelo;

public abstract class Obrok {

	String naziv;
	private double cena;

	public Obrok() {
		this.cena = Math.random() * 300 + 300;
	}

	public String getNaziv() {
		return naziv;
	}

	public double getCena() {
		return cena;
	}

}
