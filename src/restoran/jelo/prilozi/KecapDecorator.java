package restoran.jelo.prilozi;

import restoran.jelo.Obrok;

public class KecapDecorator extends ObrokDecorator{

	public KecapDecorator(Obrok obrok) {
		super(obrok);
	}
	
	public double getCena() {
		return super.getCena() + Math.random() * 80 + 20;
	}

}
