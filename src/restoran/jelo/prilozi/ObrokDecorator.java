package restoran.jelo.prilozi;

import restoran.jelo.Obrok;

public abstract class ObrokDecorator extends Obrok {

	private Obrok obrok;

	public ObrokDecorator(Obrok obrok) {
		this.obrok = obrok;
	}

	public double getCena() {
		return obrok.getCena();
	}

}
