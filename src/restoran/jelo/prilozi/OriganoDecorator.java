package restoran.jelo.prilozi;

import restoran.jelo.Obrok;

public class OriganoDecorator extends ObrokDecorator{

	public OriganoDecorator(Obrok obrok) {
		super(obrok);
	}
	
	public double getCena() {
		return super.getCena() + Math.random() * 80 + 20;
	}

}
