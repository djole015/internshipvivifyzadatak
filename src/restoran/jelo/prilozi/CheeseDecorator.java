package restoran.jelo.prilozi;

import restoran.jelo.Obrok;

public class CheeseDecorator extends ObrokDecorator{

	public CheeseDecorator(Obrok obrok) {
		super(obrok);
	}

	public double getCena() {
		return super.getCena() + Math.random() * 80 + 20;
	}
}
