package restoran.jelo;


public enum ObrokType {
	PIZZA{
		public Obrok getObrok(String naziv) {
			return new Pizza(naziv);
		}
	},
	PASTA{
		public Obrok getObrok(String naziv) {
			return new Pasta(naziv);
		}
	};
	
	abstract Obrok getObrok(String naziv);
}
