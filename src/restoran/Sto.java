package restoran;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import restoran.jelo.Obrok;
import restoran.napitak.Pice;

public class Sto {

	private int brojStola;

	private List<Porudzbina> porudzbine = new ArrayList<Porudzbina>();

	public Sto(int brojStola) {
		super();
		this.brojStola = brojStola;
	}

	public int getBrojStola() {
		return brojStola;
	}

	public void setBrojStola(int brojStola) {
		this.brojStola = brojStola;
	}

	public List<Porudzbina> getPorudzbine() {
		return porudzbine;
	}

	public void poruci(Porudzbina porudzbina) throws Exception {
		if (getPorudzbine().isEmpty() || getPorudzbine().get(getPorudzbine().size() - 1).isPaid()) {
			porudzbine.add(porudzbina);
			porudzbina.setPaid(false);
			System.out.println("Porudzbina: datum " + new Date() + " sto broj " + getBrojStola());
			return;
		}

		throw new Exception("Nije moguce izdati novu porudzbinu jer prethodna nije placena");
	}

	public void naplati(Porudzbina porudzbina) {
		double cena = 0;

		for (Obrok obrok : porudzbina.getObroci()) {
			cena += obrok.getCena();
		}

		for (Pice p : porudzbina.getPica()) {
			cena += p.getCena();
		}

		porudzbina.setPaid(true);

		System.out.println("Racun: datum " + new Date() + " sto broj " + getBrojStola() + ", " + cena);

	}

}
