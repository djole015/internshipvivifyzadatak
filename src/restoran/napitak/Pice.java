package restoran.napitak;

public class Pice {

	String naziv;
	private double cena;
	double zapremina;

	public Pice() {
		this.cena = Math.random() * 350 + 150;
	}

	public double getZapremina() {
		return zapremina;
	}

	public String getNaziv() {
		return naziv;
	}

	public double getCena() {
		return cena;
	}

}
