package restoran.napitak;

public class PiceFactory {
	
	public static Pice createPice(PiceType PiceType, String naziv, double zapremina) {
		return PiceType.getPice(naziv, zapremina);
	}
}
