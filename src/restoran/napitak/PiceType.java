package restoran.napitak;


public enum PiceType {
	VODA{
		public Pice getPice(String naziv, double zapremina) {
			return new Voda(naziv, zapremina);
		}
	},
	GAZIRANI_SOK{
		public Pice getPice(String naziv, double zapremina) {
			return new GaziraniSok(naziv, zapremina);
		}
	},
	NEGAZIRANI_SOK{
		public Pice getPice(String naziv, double zapremina) {
			return new NegaziraniSok(naziv, zapremina);
		}
	};
	
	abstract Pice getPice(String naziv, double zapremina);
}
