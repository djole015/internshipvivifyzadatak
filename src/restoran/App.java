package restoran;

import java.util.Arrays;

import restoran.jelo.Obrok;
import restoran.jelo.ObrokFactory;
import restoran.jelo.ObrokType;
import restoran.jelo.prilozi.CheeseDecorator;
import restoran.jelo.prilozi.KecapDecorator;
import restoran.jelo.prilozi.OriganoDecorator;
import restoran.napitak.Pice;
import restoran.napitak.PiceFactory;
import restoran.napitak.PiceType;

public class App {

	public static void main(String[] args) throws Exception {


		Sto sto1 = new Sto(1);
		Sto sto2 = new Sto(2);
		Sto sto3 = new Sto(3);
		Sto sto4 = new Sto(4);
		
		Obrok pizza11 = new OriganoDecorator(new KecapDecorator(ObrokFactory.createObrok(ObrokType.PIZZA, "Capricozza")));
		Obrok pasta12 = new CheeseDecorator(ObrokFactory.createObrok(ObrokType.PASTA, "Italiana"));
		Pice cocaCola11 = PiceFactory.createPice(PiceType.GAZIRANI_SOK, "Coca Cola", 0.5);
		Pice cocaCola12 = PiceFactory.createPice(PiceType.GAZIRANI_SOK, "Coca Cola", 0.5);
		
		Obrok pizza21 = ObrokFactory.createObrok(ObrokType.PIZZA, "Siciliana");
		Obrok pasta22 = ObrokFactory.createObrok(ObrokType.PASTA, "Carbonara");
		Pice negaziraniSok21 = PiceFactory.createPice(PiceType.NEGAZIRANI_SOK, "Gusti sok", 0.25);
		
		Obrok pizza31 = new KecapDecorator(new KecapDecorator(ObrokFactory.createObrok(ObrokType.PIZZA, "Capricozza")));
		Obrok pizza32 = new KecapDecorator(new KecapDecorator(ObrokFactory.createObrok(ObrokType.PIZZA, "Capricozza")));
		Obrok pizza33 = new KecapDecorator(new KecapDecorator(ObrokFactory.createObrok(ObrokType.PIZZA, "Capricozza")));
		Pice negaziraniSok31 = PiceFactory.createPice(PiceType.NEGAZIRANI_SOK, "Gusti sok", 0.25);
		Pice gaziraniSok31 = PiceFactory.createPice(PiceType.GAZIRANI_SOK, "Gazirani sok", 0.3);
		Pice voda31 = PiceFactory.createPice(PiceType.VODA, "voda", 0.25);
		
		Porudzbina porudzbina1 = new Porudzbina(sto1);
		Obrok[] obroci1 = {pizza11, pasta12};
		Pice[] pica1 = {cocaCola11, cocaCola12};
		porudzbina1.setObroci(Arrays.asList(obroci1));
		porudzbina1.setPica(Arrays.asList(pica1));
		sto1.poruci(porudzbina1);
		
		Porudzbina porudzbina2 = new Porudzbina(sto2);
		Obrok[] obroci2 = {pizza21, pasta22};
		Pice[] pica2 = {negaziraniSok21};
		porudzbina2.setObroci(Arrays.asList(obroci2));
		porudzbina2.setPica(Arrays.asList(pica2));
		sto2.poruci(porudzbina2);
		
		Porudzbina porudzbina3 = new Porudzbina(sto3);
		Obrok[] obroci3 = {pizza31, pizza32, pizza33};
		Pice[] pica3 = {negaziraniSok31, gaziraniSok31, voda31};
		porudzbina3.setObroci(Arrays.asList(obroci3));
		porudzbina3.setPica(Arrays.asList(pica3));
		sto3.poruci(porudzbina3);
		
		sto1.naplati(porudzbina1);
		sto3.naplati(porudzbina3);
		sto2.naplati(porudzbina2);
		
		Porudzbina porudzbina = new Porudzbina(sto2);
		Obrok pizza = ObrokFactory.createObrok(ObrokType.PIZZA, "Capricozza");
		Obrok[] obroci = {pizza};
		porudzbina.setObroci(Arrays.asList(obroci));
		sto2.poruci(porudzbina);
	}

}
